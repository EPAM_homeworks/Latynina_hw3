from collections import deque
import networkx as nx
import matplotlib.pyplot as plt
%matplotlib inline
        
class Graph:
    def __init__(self, e):
        self.e = e
    def __getitem__(self, item):
        return self.e[item]

class GraphIterator:
    def __init__(self, g, start_v):
        self.graph = g
        self.start_v = start_v
        self.visited = set()
        self.queue = deque()
        self.queue.append(self.start_v)
        
    def __iter__(self):
        return self
    
    def has_next(self) -> bool:
        return True if self.queue else False

    def __next__(self) -> str:
        if self.has_next():
            vertex = self.queue.popleft()
            self.visited.add(vertex)
            for nxt in self.graph[vertex]:
                if nxt not in self.visited and nxt not in self.queue:
                    self.queue.append(nxt)
            return vertex
        raise StopIteration

        
# TEST AND VISUALIZE

E = {"A": ["B", "C"], 
     "B": ["A", "C", "D", "E"], 
     "C": ["A", "B", "E"], 
     "D": ["B"], 
     "E": ["B", "C"]}

start_v = "A"

G = nx.Graph(E)
iterator = GraphIterator(Graph(E), start_v)
for vertex in iterator:
    print('The vertex now is ', vertex)
nx.draw_networkx(G, with_labels=True)
plt.show()
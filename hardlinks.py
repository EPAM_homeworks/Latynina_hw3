from os import listdir,stat,getcwd

def hardlink_check(directory_path: str) -> bool:
    try:
        path = getcwd() + directory_path
        directory = listdir(path)
    except FileNotFoundError:
        return 'The directory is wrong!'
    inodes = set()
    for name in directory:
        inode = stat(path+'/'+name).st_ino
        if node in inodes:
            return True
        else:
            inodes.add(inode)
    else:
        return False
import itertools
import numpy as np
import os
import tempfile

def merge_files(fname1: str, fname2: str) -> str:
    common = []
    f2 = open(fname2, 'r')
    res = tempfile.NamedTemporaryFile(mode='w', delete=False, prefix='file_result_', 
                                      suffix='.txt', dir=os.getcwd())
    f1 = open(fname1, 'r')
    for num1, num2 in itertools.zip_longest(f1, f2):
        if num2 != None and num1 != None:
            num1 = int(num1)
            num2 = int(num2)
            if num1<num2:
                res.write(str(num1)+'\n'+str(num2)+'\n')
            else:
                res.write(str(num2)+'\n'+str(num1)+'\n')
        if num1 == None or num2 == None:
            if num1:
                res.write(str(num1)+'\n')
            if num2:
                res.write(str(num2)+'\n')

merge_files("file1.txt", "file2.txt")


# Not O(1) and O(n) variant
def merge_files(fname1: str, fname2: str) -> str:
    common = []
    for fname in [fname1, fname2]:
        with open(fname, 'r') as f1:
            nums = list(map(str.strip, f1.readlines()))
            nums = list(map(int, nums))
            common.append(nums)
        f1.close()
    common = sorted(np.unique(list(itertools.chain.from_iterable(common))))
    res = tempfile.NamedTemporaryFile(mode='w', delete=False, prefix='file_result_', 
                                      suffix='.txt', dir=os.getcwd())
    for element in common:
        res.writelines(str(element)+'\n')
    res.close()

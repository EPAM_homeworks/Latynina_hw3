def infix_postfix(tokens):
    output = []
    stack = []
    ops = ['/', '*', '-', '+']
    prec = { "+": 0, "-": 0, "*": 0, "/": 0, '(': 0, ')':0}
    for item in tokens:
        if item in ops:
            while stack and prec[stack[-1]] >= prec[item]:
                output.append(stack.pop())
            stack.append(item)
        elif item == "(":
            stack.append("(")
        elif item == ")":
            while stack and stack[-1] != "(":
                output.append(stack.pop())
        else:
            output.append(item)
    while stack:
        output.append(stack.pop())
    return output


def postfix_infix(input_string: str) -> str:
    stack = []
    for element in input_string:
        if element.isalpha():
            stack.append([element, 'single'])
        if element in '+-':
            a = stack.pop()
            b = stack.pop()
            if element == '-' and a[1] == 'low':
                a[0] = a[0] = '(' + a[0] + ')'
            stack.append([b[0] + element + a[0], 'low'])
        if element in '*/':
            a = stack.pop()
            b = stack.pop()
            if a[1] in 'low' or (element == '/' and a[1] == 'high'):
                a[0] = '(' + a[0] + ')'
            if b[1] == 'low':
                b[0] = '(' + b[0] + ')'
            stack.append([b[0] + element + a[0], 'high'])
    return stack.pop()[0]

def brackets_trim(input_data: str) -> str:
    rpn = infix_postfix(input_data)
    return postfix_infix(rpn)

input_data = ["(a*(b/c))", "a", "a+(b+c)"]
for data in input_data:
    print(brackets_trim(data))